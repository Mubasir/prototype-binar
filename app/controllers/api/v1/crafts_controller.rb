module Api
  module V1

    class CraftsController < ApplicationController
      protect_from_forgery with: :null_session

      def index
        @crafts = Craft.all
        render json: {status: "Success", message: "Loaded Crafts", data: @crafts}, status: :ok
      end

      def show
        @craft = Craft.find(params[:id])
        render json: {status: "Success", message: "Loaded Crafts", data: @craft}, status: :ok
      end

      def create
        @craft = Craft.new(craft_params)

        if @craft.save
          render json: {status: "Berhasil", message: "Craft Saved", data: @craft}, status: :ok
        else
          render json: {status: "Error", message: "Craft can't be saved", data: @craft.errors}, status: :unprocessable_entity
        end
      end

      def update
        @craft = Craft.find(params[:id])

        if @craft.update_attributes(craft_params)
          render json: {status: "Berhasil", message: "Craft Updated", data: @craft}, status: :ok
        else
          render json: {status: "Berhasil", message: "Craft can't be Updated", data: @craft.errors}, status: :unprocessable_entity
        end
      end

      def destroy
        @craft = Craft.find(params[:id])

        
      end

      private

      def craft_params
        params.permit(:name, :description)
      end
    end

  end
end