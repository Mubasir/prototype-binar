Rails.application.routes.draw do
  
  namespace 'api' do
    namespace 'v1' do
      resources :crafts
    end
  end

  resources :crafts
  resources :locations
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
